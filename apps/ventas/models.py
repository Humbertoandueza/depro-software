from django.db import models
from apps.core.models import User
from apps.alevinaje.models import Alevinaje
from apps.camada.models import Especie
# Create your models here.
class Venta(models.Model):
	fecha = models.DateField(auto_now_add=True)
	productor = models.ForeignKey(User,verbose_name="Nombre del Productor")
	#administrador = models.ForeignKey(User,verbose_name="Nombre del Administrador")
	total = models.IntegerField(verbose_name="Total",default=0)
	estado = (
			("Procesada", "Procesada"),
			("Rechazada","Rechazada"),
			("En espera","En espera"),

		)
	estatus = models.CharField(max_length=100, choices=estado,default="En espera")

	def __str__(self):
		return '{},{}'.format(self.productor.username,self.fecha)


class VentaDetalle(models.Model):
	especie = models.ForeignKey(Alevinaje)
	venta = models.ForeignKey(Venta)
	precio = models.IntegerField()
	subtotal = models.IntegerField()

	cantidad = models.IntegerField()

	def __str__(self):
		return '{},{},{}'.format(self.especie,self.venta.productor.username,self.precio)
