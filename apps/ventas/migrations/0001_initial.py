# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2019-05-21 14:53
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('alevinaje', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Venta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(auto_now_add=True)),
                ('total', models.IntegerField(default=0, verbose_name='Total')),
                ('estatus', models.CharField(choices=[('Procesada', 'Procesada'), ('Rechazada', 'Rechazada'), ('En espera', 'En espera')], default='En espera', max_length=100)),
                ('productor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Nombre del Productor')),
            ],
        ),
        migrations.CreateModel(
            name='VentaDetalle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('precio', models.IntegerField()),
                ('subtotal', models.IntegerField()),
                ('cantidad', models.IntegerField()),
                ('especie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='alevinaje.Alevinaje')),
                ('venta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ventas.Venta')),
            ],
        ),
    ]
