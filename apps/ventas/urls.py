from django.conf.urls import url
from .views import *

urlpatterns = [

    #vitamina 

    url(r'^list/alevines/$', ListAlevinaje.as_view(),name="alevines_list"),
    url(r'^remove/alevines/$', RemoveCarrito.as_view(),name="alevines_remove"),
    url(r'^pedido/alevines/$', RealizarPedido.as_view(),name="alevines_pedido"),
    url(r'^ver/alevines/(?P<pk>\d+)/$', DetailVenta.as_view(),name="alevines_detail"),
    url(r'^list/mis_pedidos/$', ListVentasProductor.as_view(),name="mis_pedidos"),
    url(r'^list/ventas/$', ListVentas.as_view(),name="ventas_list"),








    #url(r'^panel/$', Panel.as_view(),name="panel"),

]
