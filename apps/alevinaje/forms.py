#!/usr/bin/python
# -*- coding: utf-8 -*-
from django import forms

from .models import *
from apps.camada.models import Estatus,Estanque,Camada



class VitaminaForm(forms.ModelForm):
    class Meta:
        model= Vitamina
        fields=("__all__")

        error_messages = {
            'codigo': {
                'unique': 'Este codigo ya existe,Por Favor registre otro.'
            },
        }


class AlevinajeForm(forms.ModelForm):
	class Meta:
		model= Alevinaje
		fields=("camada","vitamina","estanque")
		widgets = {
			'camada':forms.Select(attrs={'class':'form-control'}),
			'vitamina':forms.Select(attrs={'class':'form-control'}),
			'estanque':forms.Select(attrs={'class':'form-control'}),
        }
	
	def __init__(self, *args, **kwargs):
		super(AlevinajeForm, self).__init__(*args, **kwargs)
		self.fields['estanque'].queryset = Estanque.objects.filter(estatus="Disponible")
		self.fields['camada'].queryset = Camada.objects.filter(estatus=2)



class AlevinajeUpdateForm(forms.ModelForm):
    class Meta:
        model= Alevinaje
        fields=("estatus",)

        widgets = {
        'estatus':forms.Select(attrs={'class':'form-control'}),

        }
    def __init__(self, *args, **kwargs):
        super(AlevinajeUpdateForm, self).__init__(*args, **kwargs)
        self.fields['estatus'].queryset = Estatus.objects.exclude(estatus="En formación")