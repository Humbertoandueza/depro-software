from django.db import models
#Importo el modelo Camada
from apps.camada.models import Camada,Estatus,Estanque
# Create your models here.
class Vitamina(models.Model):
	codigo = models.IntegerField(verbose_name="Codigo de Vitamina",unique=True)
	nombre = models.CharField(max_length=200,verbose_name="Nombre de la vitamina")
	cantidad = models.CharField(max_length=200,verbose_name="Cantidad de la vitamina")
	contenido = models.CharField(max_length=400,verbose_name="Contenido de la vitamina")

	def __str__(self):
		return self.nombre

class Alevinaje(models.Model):
	fecha = models.DateField(auto_now_add=True)
	camada = models.ForeignKey(Camada)
	vitamina = models.ForeignKey(Vitamina)
	estatus = models.ForeignKey(Estatus,default=2)
	vendido = models.BooleanField(default=False)
	estanque = models.ForeignKey(Estanque)
	cantidad = models.IntegerField(verbose_name="Cantidad de alevines disponibles",default=0)
	def __str__(self):
		return self.camada.especie.nombre


