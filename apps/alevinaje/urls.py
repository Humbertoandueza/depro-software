from django.conf.urls import url
from .views import *

urlpatterns = [

    #vitamina 
    url(r'^create/vitamina/$', CreateVitamina.as_view(),name="vitamina_create"),
    url(r'^list/vitamina/$', ListVitamina.as_view(),name="vitamina_list"),
    url(r'^update/vitamina/(?P<pk>\d+)/$', UpdateVitamina.as_view(),name="vitamina_update"),
    url(r'^delete/vitamina/(?P<pk>\d+)/$', DeleteVitamina.as_view(),name="vitamina_delete"),

    #Alevinaje 
    url(r'^create/alevinaje/$', CreateAlevinaje.as_view(),name="alevinaje_create"),
    url(r'^list/alevinaje/$', ListAlevinaje.as_view(),name="alevinaje_list"),
    url(r'^list/alevinaje_l/$', ListAlevinaje_l.as_view(),name="alevinaje_list_l"),
    url(r'^list/alevinaje_p/$', ListAlevinaje_p.as_view(),name="alevinaje_list_p"),

    url(r'^update/alevinaje/(?P<pk>\d+)/$', UpdateAlevinaje.as_view(),name="alevinaje_update"),
    url(r'^delete/alevinaje/(?P<pk>\d+)/$', DeleteAlevinaje.as_view(),name="alevinaje_delete"),




    #url(r'^panel/$', Panel.as_view(),name="panel"),

]
