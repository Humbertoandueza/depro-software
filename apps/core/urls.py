from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', Landing.as_view(),name="landing"),
    url(r'^panel/$', Panel.as_view(),name="panel"),
    url(r'^panel/notificaciones/$', ListNotificaciones.as_view(),name="notificaciones"),


]
