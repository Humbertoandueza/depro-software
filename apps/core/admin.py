from django.contrib import admin
from .models import  User
# Register your models here.

from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from .models import User,Notificacion


class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('cedula','email','first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser','is_productor','is_administrador',
                                       'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    limited_fieldsets = (
        (None, {'fields': ('email','username',)}),
        ('Personal info', {'fields': ('first_name', 'last_name')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('cedula','first_name','last_name','email','estados','username', 'password1', 'password2','is_productor')}
        ),
    )
    change_password_form = auth_admin.AdminPasswordChangeForm
    list_display = ('cedula','username','first_name', 'estados', 'is_superuser','is_productor','is_administrador')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('email',)
    readonly_fields = ('last_login', 'date_joined',)

admin.site.register(User,UserAdmin)
admin.site.register(Notificacion)


