from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from .utils import Estados

from django.contrib.auth.base_user import BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self, username, password=None, **kwargs):
        if not username:
            raise ValueError('Users must have a valid username.')

        account = self.model(
            username=self.model.normalize_username(username)
        )

        account.set_password(password)
        account.save()

        return account

    def create_superuser(self, username, password, **kwargs):
        account = self.create_user(username, password, **kwargs)

        account.is_superuser = True
        account.is_staff = True
        account.save()

        return account


class User(AbstractBaseUser, PermissionsMixin):
	cedula = models.IntegerField(verbose_name="Cedula",unique=True,blank=True,null=True)
	email = models.EmailField(_('Correo Electronico'), unique=True,blank=True)
	first_name = models.CharField(_('Nombre'), max_length=30, blank=True)
	last_name = models.CharField(_('Apellido'), max_length=30, blank=True)
	date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
	is_active = models.BooleanField(_('Activo'), default=True)
	is_superuser = models.BooleanField(_('Superusuario'), default=False)
	is_administrador = models.BooleanField(_('Administrador'), default=False)
	is_productor = models.BooleanField(_('Productor'), default=False)

	is_staff = models.BooleanField(_('Staff'), default=True)
	username = models.CharField(_('Nombre de Usuario'), max_length=40, unique=True)

	estados = models.CharField(_('Estado'),max_length=200,choices=Estados(),blank=True)
	objects = UserManager()

	USERNAME_FIELD = 'username'
	REQUIRED_FIELDS = []

	class Meta:
		verbose_name = _('user')
		verbose_name_plural = _('users')

	def get_full_name(self):
		full_name = '%s %s' % (self.first_name, self.last_name)
		return full_name.strip()
	
	def get_short_name(self):
		return self.first_name


class Notificacion(models.Model):
	usuario= models.ForeignKey(User,null=True,blank=True)
	contenido = models.CharField(max_length=800)
	tip = (
		("productor","productor"),
		("administrador","administrador"),
	)
	url = models.IntegerField()
	tipo = models.CharField(max_length=100, choices=tip)
	estatus = models.BooleanField(default=False)
	fecha = models.DateTimeField(auto_now_add=True)
	def __str__(self):
		return '{},{}'.format(str(self.usuario),self.estatus)