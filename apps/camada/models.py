from django.db import models

# Create your models here.
class Especie(models.Model):
	nombre = models.CharField(max_length=100,verbose_name="Nombre de la especie")
	taza_m = models.IntegerField(verbose_name="Taza de mortalidad")
	taza_s = models.IntegerField(verbose_name="Taza de sobrevivencia")
	ovas = models.IntegerField(verbose_name="Cantidad de Ovas Aproximadas")
	precio = models.IntegerField(verbose_name="Precio unitario")

	def __str__(self):
		return '{},{}'.format(self.nombre,self.precio)

class Alimento(models.Model):
	nombre = models.CharField(max_length=100,verbose_name="Nombre del Alimento")
	tipo = models.CharField(max_length=100,verbose_name="Tipo de Alimento")
	peso = models.CharField(max_length=100,verbose_name="Peso neto del Alimento")

	def __str__(self):
		return self.nombre

class Estanque(models.Model):
	numero = models.CharField(max_length=200,verbose_name="Numero del Estanque")
	medida = models.CharField(max_length=200,verbose_name="Medida del Estanque")
	tipo = models.CharField(max_length=200,verbose_name="Tipo de estanque")
	fecha = models.DateField(verbose_name='Fecha de movimiento',null=True,blank=True) 
	estado = (
			("Ocupado", "Ocupado"),
			("Disponible","Disponible"),
			("Mantenimiento","Mantenimiento"),
		)
	estatus = models.CharField(max_length=100, choices=estado,default="Disponible")
	def __str__(self):
		return str(self.tipo)+'-'+str(self.numero)

class Estatus(models.Model):
	estatus = models.CharField(max_length=200,verbose_name="Estatus")
	
	def __str__(self):
		return self.estatus

class Camada(models.Model):
	fecha = models.DateField(auto_now_add=True)
	cantidad_h = models.IntegerField(verbose_name="Cantidad ejemplares Hembras")
	cantidad_k = models.IntegerField(verbose_name="Peso total de ejemplares Hembras")
	cantidad_m = models.IntegerField(verbose_name="Cantidad ejemplares Machos")
	cantidad_e = models.IntegerField(verbose_name="Cantidad ejemplares esperados",null=True,blank=True)
	estanque = models.ForeignKey(Estanque)
	alimento = models.ForeignKey(Alimento)
	especie = models.ForeignKey(Especie)
	estatus = models.ForeignKey(Estatus,default=1)

	def __str__(self):
		return '{},{},{}'.format(self.fecha,self.especie.nombre,self.estanque.numero)


class Formula(models.Model):
	porcentaje = models.IntegerField(verbose_name="Porcentaje")

	def __str__(self):
		return str(self.porcentaje)